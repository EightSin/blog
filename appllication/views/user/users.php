<h1>Users</h1>

<a class="btn btn-default" href="add">Add user</a>

<table class="table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Birthday</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user) : ?>
            <tr>
                <td><a href="edit/<?=$user['id']?>"><?=$user['first_name']?> <?=$user['last_name']?></a></td>
                <td><?=$user['email']?></td>
                <td>

                    <?php $birthday = $user['birthday'];
                          $date = new DateTime($birthday);
                          echo $new_date = $date->format('l M Y');
                    ?>
                </td>
                <td><a href="delete/<?=$user['id']?>">Delete</a></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>