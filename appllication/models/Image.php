<?php

use \Framework\Model;

class Image extends Model {
    private $tableName = 'image';

    public function save($imagePath, $postId)
    {
        $sql = "INSERT INTO {$this->tableName} (
    		path,
    		post_id) 
    		VALUES (?,?)";

        $query = $this->connect->prepare($sql);
        $query->execute([$imagePath,
            $postId]);
    }
}